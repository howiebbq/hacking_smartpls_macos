# SmartPLS_MacOS

> Goal: gen an extension on the license of SmartPLS from a professional free license 

![Build Status](https://travis-ci.org/joemccann/dillinger.svg?branch=master)
![Build Status](https://img.shields.io/github/release/pandao/editor.md.svg?branch=master)


# HWID
- read Wikipedia

## An extension files?
- created by me

included
`SmartPLSCR.app`


### How I have an extension file? 
- You must provide information about time "date-month-year" that you did activation key in the first time. 

### If I don't remember exactly. Is it possible to create extension files? 
- Impossible. Look forward HWID part 

## New features on extension files
- Easy update new version from SmartPLS release (manually by hand)
- Internet access without delay
- Running smooth
- Keep orgin file of SmartPLS
- No modify anything
- Without harming MacOS
- No virus

## Important note for MacOS
grant a user `Administration privileges` to run an extension file

<img src="admin.png"/>

## Installation

<img src="setup.png"/>

<img src="run.png"/>

# Development
Want to contribute? No. Thank you.

# Contact me
Email: `howie[at]tungdao.org`